<?php

namespace Drupal\social_auth_pinterest\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines the Pinterest Auth interface.
 */
interface PinterestAuthInterface extends NetworkInterface
{
}
