<?php

namespace Drupal\social_auth_pinterest\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\social_auth\Form\SocialAuthSettingsForm;

/**
 * Settings form for Social Auth Pinterest.
 */
class PinterestAuthSettingsForm extends SocialAuthSettingsForm
{

     /**
      * {@inheritdoc}
      */
     public function getFormId()
     {
          return 'social_auth_pinterest_settings';
     }

     /**
      * {@inheritdoc}
      */
     protected function getEditableConfigNames()
     {
          return array_merge(
               parent::getEditableConfigNames(),
               ['social_auth_pinterest.settings']
          );
     }

     /**
      * {@inheritdoc}
      */
     public function buildForm(array $form, FormStateInterface $form_state)
     {
          $config = $this->config('social_auth_pinterest.settings');

          $form['pinterest_settings'] = [
               '#type' => 'details',
               '#title' => $this->t('Pinterest Client settings'),
               '#open' => TRUE,
               '#description' => $this->t(
                    'You need to first create a Pinterest App at <a href="@pinterest-dev">@pinterest-dev</a>',
                    ['@pinterest-dev' => 'https://developers.pinterest.com/apps/']
               ),
          ];

          $form['pinterest_settings']['client_id'] = [
               '#type' => 'textfield',
               '#required' => TRUE,
               '#title' => $this->t('Client ID'),
               '#default_value' => $config->get('client_id'),
               '#description' => $this->t('Copy the Client ID here.'),
          ];

          $form['pinterest_settings']['client_secret'] = [
               '#type' => 'textfield',
               '#required' => TRUE,
               '#title' => $this->t('Client Secret'),
               '#default_value' => $config->get('client_secret'),
               '#description' => $this->t('Copy the Client Secret here.'),
          ];

          $form['pinterest_settings']['authorized_redirect_url'] = [
               '#type' => 'textfield',
               '#disabled' => TRUE,
               '#title' => $this->t('Authorized redirect URIs'),
               '#description' => $this->t('Copy this value to <em>Authorized redirect URIs</em> field of your Pinterest App settings.'),
               '#default_value' => Url::fromRoute('social_auth_pinterest.callback')->setAbsolute()->toString(),
          ];

          $form['pinterest_settings']['advanced'] = [
               '#type' => 'details',
               '#title' => $this->t('Advanced settings'),
               '#open' => FALSE,
          ];

          $form['pinterest_settings']['advanced']['scopes'] = [
               '#type' => 'textarea',
               '#title' => $this->t('Scopes for API call'),
               '#default_value' => $config->get('scopes'),
               '#description' => $this->t(
                    'Define any additional scopes to be requested, separated by a comma (e.g.: username).<br>
                                  The scope \'username\' are added by default and always requested.<br>
                                  You can see the full list of valid scopes and their description <a href="@scopes">here</a>.',
                    ['@scopes' => 'https://developers.pinterest.com/docs/api/overview/#scopes']
               ),
          ];

          $form['pinterest_settings']['advanced']['endpoints'] = [
               '#type' => 'textarea',
               '#title' => $this->t('API calls to be made to collect data'),
               '#default_value' => $config->get('endpoints'),
               '#description' => $this->t('Define the endpoints to be requested when user authenticates with Pinterest for the first time<br>
                                  Enter each endpoint in different lines in the format <em>endpoint</em>|<em>name_of_endpoint</em>
                                  <b>For instance:</b><br>
                                  /v1/me/|me<br>'),
          ];

          return parent::buildForm($form, $form_state);
     }

     /**
      * {@inheritdoc}
      */
     public function submitForm(array &$form, FormStateInterface $form_state)
     {
          $values = $form_state->getValues();
          $this->config('social_auth_pinterest.settings')
               ->set('client_id', trim($values['client_id']))
               ->set('client_secret', trim($values['client_secret']))
               ->set('scopes', $values['scopes'])
               ->set('endpoints', $values['endpoints'])
               ->save();

          parent::submitForm($form, $form_state);
     }
}
