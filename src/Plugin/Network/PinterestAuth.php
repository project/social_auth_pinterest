<?php

namespace Drupal\social_auth_pinterest\Plugin\Network;

use Drupal\Core\Url;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth\Plugin\Network\NetworkBase;
use Drupal\social_auth_pinterest\Settings\PinterestAuthSettings;
use League\OAuth2\Client\Provider\Pinterest;

/**
 * Defines a Network Plugin for Social Auth Pinterest.
 *
 * @package Drupal\social_auth_pinterest\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_pinterest",
 *   social_network = "Pinterest",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_pinterest\Settings\PinterestAuthSettings",
 *       "config_id": "social_auth_pinterest.settings"
 *     }
 *   }
 * )
 */
class PinterestAuth extends NetworkBase implements PinterestAuthInterface
{

     /**
      * Sets the underlying SDK library.
      *
      * @return League\OAuth2\Client\Provider\Pinterest
      *   The initialized 3rd party library instance.
      *
      * @throws \Drupal\social_api\SocialApiException
      *   If the SDK library does not exist.
      */
     protected function initSdk()
     {

          $class_name = 'League\OAuth2\Client\Provider\Pinterest';
          if (!class_exists($class_name)) {
               throw new SocialApiException(sprintf('The Pinterest Library for the league oAuth not found. Class: %s.', $class_name));
          }

          /** @var \Drupal\social_auth_pinterest\Settings\PinterestAuthSettings $settings */
          $settings = $this->settings;

          if ($this->validateConfig($settings)) {
               // All these settings are mandatory.
               $league_settings = [
                    'clientId' => $settings->getClientId(),
                    'clientSecret' => $settings->getClientSecret(),
                    'redirectUri' => Url::fromRoute('social_auth_pinterest.callback')->setAbsolute()->toString(),
               ];

               // Proxy configuration data for outward proxy.
               $proxyUrl = $this->siteSettings->get('http_client_config')['proxy']['http'];
               if ($proxyUrl) {
                    $league_settings['proxy'] = $proxyUrl;
               }

               return new Pinterest($league_settings);
          }

          return FALSE;
     }

     /**
      * Checks that module is configured.
      *
      * @param \Drupal\social_auth_pinterest\Settings\PinterestAuthSettings $settings
      *   The Pinterest auth settings.
      *
      * @return bool
      *   True if module is configured.
      *   False otherwise.
      */
     protected function validateConfig(PinterestAuthSettings $settings)
     {
          $client_id = $settings->getClientId();
          $client_secret = $settings->getClientSecret();
          if (!$client_id || !$client_secret) {
               $this->loggerFactory
                    ->get('social_auth_pinterest')
                    ->error('Define Client ID and Client Secret on module settings.');

               return FALSE;
          }

          return TRUE;
     }
}
